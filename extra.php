<?php
/**
 * Plugin Name: Webforia Extra
 * Description: extra feature for website service
 * Plugin URI:  https://webforia.id
 * Version:     1.0.0
 * Author:      Webforia Studio
 * Author URI:  https://webforia.id
 * Text Domain: webforia-extra
 */

if (!defined('ABSPATH')) {
    exit;
}

define('WEBFORIA_EXTRA_TEMPLATE', plugin_dir_path(__FILE__));
define('WEBFORIA_EXTRA_ASSETS', plugin_dir_url(__FILE__));


/*=================================================;
/* LOAD THIS PLUGIN AFTER RETHEME LOAD
/*================================================= */
function wex_plugin_loaded()
{
    // include file
    require_once WEBFORIA_EXTRA_TEMPLATE . '/includes/include.php';
    require_once WEBFORIA_EXTRA_TEMPLATE . '/plugin.php';
    require_once WEBFORIA_EXTRA_TEMPLATE . '/includes/class-templates.php';
    require_once WEBFORIA_EXTRA_TEMPLATE . '/function.php';

    // update plugin
    $check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/webforia-extra', __FILE__, 'webforia-extra');
    $check_update->setBranch('stable_release');

}

add_action('rt_after_setup_theme', 'wex_plugin_loaded');
