<div class="rt-profile rt-profile--center">

    <?php if($data->author_background): ?>
    <div class="rt-profile__background rt-img rt-img--background " style="background-image: url(<?php echo  $data->author_background ?>)"></div>
    <?php endif ?>

  <div class="rt-profile__thumbnail rt-img">
      <?php
      if(!empty($data->author_avatar)):
        echo wp_get_attachment_image($data->author_avatar, 'thumbnail');
      else:
        echo $data->author_gravatar;
      endif;?>
  </div>

  <div class="rt-profile__body">
    <h4 class="rt-profile__title"><?php echo $data->author_name ?></h4>

    <?php if($data->author_job): ?>
    <div class="rt-profile__position"><?php echo $data->author_job ?></div>
    <?php endif ?>

    <div class="rt-profile__content">
        <?php echo $data->author_desc ?>
    </div>

    <div class="rt-profile__socmed rt-socmed rt-socmed--border">

        <?php if (!empty($data->author_fb)): ?>
        <a href="<?php echo $data->author_fb ?>" class="rt-socmed__item facebook">
            <i class="fa fa-facebook-f" aria-hidden="true"></i>
        </a>
        <?php endif?>

        <?php if (!empty($data->author_tw)): ?>
        <a href="<?php echo $data->author_tw ?>" class="rt-socmed__item twitter">
            <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <?php endif?>

        <?php if (!empty($data->author_instagram)): ?>
        <a href="<?php echo $data->author_instagram ?>" class="rt-socmed__item instagram">
            <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
        <?php endif?>

        <?php if (!empty($data->author_youtube)): ?>
        <a href="<?php echo $data->author_youtube ?>" class="rt-socmed__item youtube">
            <i class="fa fa-youtube" aria-hidden="true"></i>
        </a>
        <?php endif?>

        <?php if (!empty($data->author_linkedin)): ?>
        <a href="<?php echo $data->author_linkedin ?>" class="rt-socmed__item linkedin">
            <i class="fa fa-linkedin-in" aria-hidden="true"></i>
        </a>
        <?php endif?>
    </div>
  </div>
</div>
